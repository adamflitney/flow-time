# FlowTime
A pomodoro timer style app for focus management that uses the flow time approach as explained by @lightsandcandy

here:
https://medium.com/@lightsandcandy/the-flowtime-technique-7685101bd191

and here:
https://medium.com/@lightsandcandy/the-flowtime-technique-cheat-sheet-30168b2e31d9


The "focus" of this app is recording the task you set out to do and how long you focussed on that task before being interrupted/distracted/just stopping. Focussing on recording the time rather than giving yourself explicit timers is intended to allow periods of flow to happen more easily.
The app still allows for breaks and will recommend an appropriate length of break based on how much focussed time you achieved.

